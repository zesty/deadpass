use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(SealedSecret)]
pub fn my_macro(input: TokenStream) -> TokenStream {
    // Parse the input tokens into a syntax tree
    let input = parse_macro_input!(input as DeriveInput);
    let DeriveInput { ident, .. } = input;

    let output = quote! {
        impl deadpass::SealedSecret for #ident {}
    };
    output.into()
}
