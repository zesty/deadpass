use secrecy::Zeroize;
use sodiumoxide::crypto::hash::sha256;
use crate::crypto::Kdf;
use std::error::Error;
use secrecy::zeroize::Zeroizing;
use rand::{rngs::OsRng, RngCore};
use std::fs::{OpenOptions, File};
use std::io::{Write, Read};
use std::io::ErrorKind;

#[derive(Debug)]
pub struct ComposedKey(Zeroizing<Vec<u8>>);

impl ComposedKey {
    pub fn from_pass(pass: &[u8]) -> ComposedKey {
        let hash_buff = Zeroizing::new(ComposedKey::_from_pass(pass));
        ComposedKey(Zeroizing::new(sha256::hash(hash_buff.as_slice()).0.to_vec()))
    }

    fn _from_pass(pass: &[u8]) -> Vec<u8> {
        let mut buffer = Vec::new();
        buffer.extend_from_slice(&sha256::hash(pass).0);
        buffer
    }

    pub fn from_both(pass: &[u8], keyfile: &str) -> Result<ComposedKey, Box<dyn Error>> {
        let mut hash_buff = ComposedKey::_from_pass(pass);
        let mut file      = File::open(keyfile)?;
        let mut key_bytes = Vec::new();
        match file.read_to_end(&mut key_bytes) {
            Ok(_)  => (),
            Err(_) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error reading keyfile")))
        }
        hash_buff.extend(sha256::hash(key_bytes.as_slice()).0);
        let hash_buff = Zeroizing::new(hash_buff);
        Ok(ComposedKey(Zeroizing::new(sha256::hash(hash_buff.as_slice()).0.to_vec())))
    }

    pub fn new(pass: &[u8], keyfile_path: &str) -> Result<ComposedKey, Box<dyn Error>> {
        let mut hash_buff = ComposedKey::_from_pass(pass);
        let mut keyfile   = [0u8; 128];
        OsRng.try_fill_bytes(&mut keyfile)?;

        // saving keyfile to requested path
        let mut file = OpenOptions::new().create(true).write(true).open(keyfile_path)?;
        file.write(&keyfile)?;

        hash_buff.extend(sha256::hash(&keyfile).0);
        // cleaning keyfile
        keyfile.zeroize();

        let hash_buff = Zeroizing::new(hash_buff);
        Ok(ComposedKey(Zeroizing::new(sha256::hash(hash_buff.as_slice()).0.to_vec())))
    }

    pub(crate) fn master_key(&self, kdf: &Kdf) -> Result<MasterKey, Box<dyn Error>> {
        match kdf {
            Kdf::ARGON2ID { salt, opslimit, memlimit } => Ok(MasterKey(Zeroizing::new(crate::crypto::argon2::argon2id_hash(self.0.as_slice(), salt.to_owned(), *opslimit as usize, *memlimit as usize)?)))
        }
    }
}

pub(crate) struct MasterKey(Zeroizing<Vec<u8>>);

impl MasterKey {
    pub(crate) fn cipher_key(&self, seed: &[u8]) -> CipherKey {
        let mut data = Vec::new();
        data.extend_from_slice(seed);
        data.extend_from_slice(self.0.as_slice());
        let sec_data = Zeroizing::new(data);

        CipherKey(sha256::hash(sec_data.as_slice()).0)
    }
}

impl std::fmt::Debug for MasterKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #[cfg(feature = "visible")]
        {
            write!(f, "{:?}", &self.0)
        }

        #[cfg(not(feature = "visible"))]
        {
            write!(f, "\"*HIDDEN*\"")
        }
    }
}


pub(crate) struct CipherKey(pub(crate) [u8; 32]);
