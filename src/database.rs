use secrecy::zeroize::Zeroizing;
use std::error::Error;
use crate::crypto;
use crate::format::DeadPass;
use crate::keys::ComposedKey;
use crate::types::SealedSecret;
use crate::crypto::{EncryptCipher, KeyKdf};

use rkyv::{
    Serialize, Deserialize, Archive,
    ser::serializers::{AlignedSerializer, CompositeSerializer, BufferScratch},
    AlignedVec, Infallible, AlignedBytes
};

pub struct DbOptions {
    pub ciph: Option<EncryptCipher>,
    pub kdf: Option<KeyKdf>
}

#[derive(Debug)]
pub struct Database<T> {
    // Encrpyted database
    db: DeadPass<T>,
    // database path
    db_path: Zeroizing<String>,
}

impl<T> Database<T> where T: SealedSecret + Archive + Serialize<CompositeSerializer<AlignedSerializer<AlignedVec>, BufferScratch<AlignedBytes<4096_usize>>>>,
                          T::Archived: Deserialize<T, Infallible>  {
    /// Create a new database
    pub fn new(db: T, db_path: &str, key: ComposedKey, opt: Option<DbOptions>) -> Result<Database<T>, Box<dyn Error>> {
        crypto::init()?;

        Ok(Database {
            db: DeadPass::new(db, key, opt)?,
            db_path: Zeroizing::new(db_path.to_owned()),
        })
    }

    /// Open an existing database
    pub fn open(db_path: &str, key: ComposedKey) -> Result<Database<T>, Box<dyn Error>> {
        crypto::init()?;

        let db = Database {
            db: DeadPass::open(db_path, key)?,
            db_path: Zeroizing::new(db_path.to_owned()),
        };

        Ok(db)
    }

    /// Change keys
    /// Not applied to encrpyted database until `save()` is called afterward
    pub fn set_keys(&mut self, key: ComposedKey) -> Result<(), Box<dyn Error>> {
        self.db.set_master_key(key)
    }

    /// Change Options (kdf, cipher)
    /// Not applied to encrpyted database until `save()` is called afterward
    pub fn set_options(&mut self, opt: DbOptions) -> Result<(), Box<dyn Error>> {
        self.db.set_options(opt)
    }

    /// Get reference to database
    pub fn get(&self) -> &T {
        &self.db.1.0
    }

    /// Get mutable reference to database
    pub fn get_mut(&mut self) -> &mut T {
        &mut self.db.1.0
    }

    /// Make all new changes persistent
    pub fn save(&mut self) -> Result<(), Box<dyn Error>> {
        self.db.write(self.db_path.as_str())
    }

}
