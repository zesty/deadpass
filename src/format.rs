use rkyv::ser::Serializer;
use sodiumoxide::crypto::secretbox::gen_key;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::error::Error;
use std::io::ErrorKind;
use secrecy::Zeroize;
use rkyv::{
    archived_root,
    Serialize, Deserialize, Archive,
    ser::serializers::{AlignedSerializer, CompositeSerializer, BufferScratch},
    AlignedVec, Infallible, AlignedBytes
};

use crate::keys::{ComposedKey, MasterKey};
use crate::crypto::{Kdf, CiphFn, KeyKdf, EncryptCipher, self};
use crate::types::SealedSecret;
use crate::database::DbOptions;

#[derive(Debug)]
pub(crate) struct DeadPass<T>(
    // header
    pub(crate) Header,
    // database itself
    pub DB<T>,
    // Unprepared master key
    ComposedKey,
    // database keys
    MasterKey
    );

impl<T> DeadPass<T> where T: SealedSecret + Archive + Serialize<CompositeSerializer<AlignedSerializer<AlignedVec>, BufferScratch<AlignedBytes<4096_usize>>>>,
                    T::Archived: Deserialize<T, Infallible> {
    pub(crate) fn new(db: T, key: ComposedKey, opt: Option<DbOptions>) -> Result<DeadPass<T>, Box<dyn Error>> {
        let header = match opt {
            Some(v) => {
                let (kdf, ciph) = DeadPass::<T>::get_options(v.kdf, v.ciph);
                Header::new(None, kdf, ciph)
            },
            None    => Header::new(None, None, None)
        };
        let master_key = DeadPass::<T>::master_key(&header, &key)?;
        Ok(DeadPass(header, DB::new(db), key, master_key))
    }

    pub(crate) fn open(db_path: &str, key: ComposedKey) -> Result<DeadPass<T>, Box<dyn Error>> {
        let mut db   = File::open(db_path)?;
        let header   = Header::read(&mut db)?;

        let master_key = DeadPass::<T>::master_key(&header, &key)?;
        let db         = DB::read(&mut db, &master_key, &header.master_seed, &header.cipher)?;


        Ok(DeadPass(header, db, key, master_key))
    }

    pub(crate) fn write(&mut self, db_path: &str) -> Result<(), Box<dyn Error>> {
        let mut db         = OpenOptions::new().create(true).write(true).open(db_path)?;
        // serialize db
        type MySerializer  = CompositeSerializer<AlignedSerializer<AlignedVec>, BufferScratch<AlignedBytes<4096>>, Infallible>;
        let mut serializer = MySerializer::default();
        serializer.serialize_value(&self.1)?;
        let (serializer, h, _)  = serializer.into_components();
        let mut bytes           = serializer.into_inner();

        // sanitizing scratch used for serializing
        h.into_inner().zeroize();
        
        // encrypt db
        let (enc_bytes, ciph) = self.1.encrypt(bytes.as_ref(), &self.0.cipher, self.get_master_key(), self.0.master_seed.as_slice())?;
        bytes.as_mut().zeroize();

        self.0 = Header::new(Some(self.0.master_seed.as_slice()), Some(self.0.kdf.clone()), Some(ciph));

        self.0.write(&mut db)?;
        self.1.write(&mut db, &enc_bytes)?;

        Ok(())
    }

    pub(crate) fn set_master_key(&mut self, key: ComposedKey) -> Result<(), Box<dyn Error>> {
        self.3 = DeadPass::<T>::master_key(&self.0, &key)?;
        self.2 = key;
        Ok(())
    }

    pub(crate) fn master_key(header: &Header, key: &ComposedKey) -> Result<MasterKey, Box<dyn Error>> {
        match key.master_key(&header.kdf) {
            Ok(v)  => Ok(v),
            Err(e) => return Err( Box::new( std::io::Error::new( ErrorKind::Other, format!("Wrong pass/keyfile: {}", e) ) ) )
        }
    }

    fn get_master_key(&self) -> &MasterKey {
        &self.3
    }

    pub(crate) fn set_options(&mut self, opt: DbOptions) -> Result<(), Box<dyn Error>> {
        let (kdf, ciph) = DeadPass::<T>::get_options(opt.kdf, opt.ciph);
        match kdf {
            Some(n) => {
                self.0.kdf = n;
                // A change in key derivation
                // need to recalculate master key
                self.3 = DeadPass::<T>::master_key(&self.0, &self.2)?;
            }
            None => ()
        }
        match ciph {
            Some(n) => {
                self.0.cipher = n;
            },
            None    => ()
        }
        Ok(())
    }

    fn get_options(kdf: Option<KeyKdf>, ciph: Option<EncryptCipher>) -> (Option<Kdf>, Option<CiphFn>) {
        let kdf = match kdf {
            Some(v) => Some(v.transform()),
            None    => None
        };
        let ciph = match ciph {
            Some(v) => Some(v.transform()),
            None    => None
        };
        (kdf, ciph)
    }
}

#[derive(Debug)]
pub(crate) struct Header {
    version: [u8; 3],
    master_seed: Vec<u8>,
    kdf: Kdf,
    cipher: CiphFn
}

impl secrecy::DebugSecret for Header {}

impl Drop for Header {
    fn drop(&mut self) {
        self.version.zeroize();
        self.master_seed.zeroize();
        // rest is sanitized by itself
    }
}

impl Header {
    pub(crate) fn new(ms: Option<&[u8]>, kdf: Option<Kdf>, ciph: Option<CiphFn>) -> Header {
        Header {
            version: [0, 1, 0],
            master_seed: if ms.is_none() { gen_key().0.to_vec() } else { ms.unwrap().to_owned() },
            kdf: if kdf.is_none() { Kdf::ARGON2ID { salt: sodiumoxide::crypto::pwhash::argon2id13::gen_salt().0,
                                    opslimit: 4, memlimit: sodiumoxide::crypto::pwhash::argon2id13::MEMLIMIT_SENSITIVE.0 as u32 } } else { kdf.unwrap() },
            cipher: if ciph.is_none() { CiphFn::XCHACHA20POLY1305 { nonce_header: None } } else { ciph.unwrap() }
        }
    }

    pub(crate) fn read(db: &mut File) -> Result<Header, Box<dyn Error>> {
        let mut head = Vec::new();

        let mut hint: [u8; 10] = [0; 10];
        db.read_exact(&mut hint)?;
        if !hint.eq(b"deadpass\0\0") {
            return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error reading database: file may not be a deadpass database")))
        }

        let mut byte: [u8; 1] = [0; 1];
        while !head.ends_with(b"\0D1I2P3P4\0P5I6N7G\0") {
            db.read_exact(&mut byte)?;
            head.extend_from_slice(&byte);
        }
        for _ in 0..18 {
            head.pop();
        }

        let header: Header;
        match head[0] {
            0 => {
                header = Header {
                    version: [head[0], head[1], head[2]],
                    // only 32 byte seed supported for now
                    master_seed: head[3..=34].to_vec(),
                    // key derivation function
                    kdf: match head[35] {
                        0 => {
                            if head.len()-36 < 24 {
                                return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Argon2ID missing its parameter from header")))
                            }

                            let mut salt: [u8; 16] = [0; 16];
                            for i in 0..16 {
                                salt[i] = head[36+i];
                            }
                            
                            let mut ops = [0; 4]; 
                            ops.copy_from_slice(&head[52..=55]);
                            let mut mem = [0; 4]; 
                            mem.copy_from_slice(&head[56..=59]);

                            Kdf::ARGON2ID {
                                salt,
                                opslimit: u32::from_be_bytes(ops),
                                memlimit: u32::from_be_bytes(mem)
                            }
                        },
                        _ => return Err(Box::new(std::io::Error::new(ErrorKind::Other, format!("Unknewn deadpass kdf function with code {}", head[35]))))
                    },
                    cipher: match head[60] {
                        0 => {
                            if head.len()-61 != 24 {
                                return Err(Box::new(std::io::Error::new(ErrorKind::Other, "xchacha20poly1305 missing nonce header")))
                            }
                            let mut nonce_header = [0; 24];
                            nonce_header.copy_from_slice(&head[61..=84]);
                            CiphFn::XCHACHA20POLY1305 { nonce_header: Some(nonce_header) }
                        },
                        _ => return Err(Box::new(std::io::Error::new(ErrorKind::Other, format!("Unknewn deadpass cipher function with code {}", head[60]))))
                    }
                };
            },
            _ => return Err(Box::new(std::io::Error::new(ErrorKind::Other, format!("Unknewn deadpass major version {}", head[0]))))
        };
        // Zeroing out all header
        head.zeroize();

        Ok(header)
    }

    pub(crate) fn write(&self, db: &mut File) -> Result<(), Box<dyn Error>> {
        db.write_all(b"deadpass\0\0")?;
        db.write_all(&[self.version[0], self.version[1], self.version[2]])?;

        db.write_all(self.master_seed.as_slice())?;
        match self.kdf {
            Kdf::ARGON2ID { salt, opslimit, memlimit } => {
                db.write_all(&[0])?;
                db.write_all(&salt)?;
                let mut ops = u32::to_be_bytes(opslimit);
                let mut mem = u32::to_be_bytes(memlimit);
                db.write_all(&ops)?;
                db.write_all(&mem)?;

                ops.zeroize();
                mem.zeroize();
            },
        };

        match self.cipher {
            CiphFn::XCHACHA20POLY1305 { nonce_header } => {
                db.write_all(&[0])?;
                let mut head = nonce_header.unwrap();
                db.write_all(&head)?;
                head.zeroize();
            }
        }

        db.write(b"\0D1I2P3P4\0P5I6N7G\0")?;

        Ok(())
    }
}

/// DeadPass database
#[derive(Archive, Deserialize, Serialize)]
#[derive(Debug)]
pub(crate) struct DB<T>(pub T);

impl<T> DB<T> where T: SealedSecret + Archive + Serialize<CompositeSerializer<AlignedSerializer<AlignedVec>, BufferScratch<AlignedBytes<4096_usize>>>>,
                    T::Archived: Deserialize<T, Infallible> {
    pub(crate) fn new(db: T) -> DB<T> {
        DB(db)
    }

    pub(crate) fn read(db: &mut File, master_key: &MasterKey, master_seed: &[u8], cipher: &CiphFn) -> Result<DB<T>, Box<dyn Error>> {
        let mut enc_bytes = Vec::new();
        db.read_to_end(&mut enc_bytes)?;


        let cipher_key     = master_key.cipher_key(master_seed);
        let mut dec_bytes  = crypto::cipher::decrypt(&enc_bytes, &cipher_key.0, &cipher)?;

        let archived = unsafe { archived_root::<DB<T>>(&dec_bytes[..]) };
        let unc_db: DB<T> = archived.deserialize(&mut Infallible)?;


        // delete everytihng
        dec_bytes.zeroize();
        // TODO: should this be removed? unecessary but we will zero out encrypted data
        enc_bytes.zeroize();

        Ok(unc_db)
    }

    pub(crate) fn encrypt(&self, bytes: &[u8], cipher: &CiphFn, master_key: &MasterKey, master_seed: &[u8]) -> Result<(Vec<u8>, CiphFn), Box<dyn Error>> {
        let cipher_key = master_key.cipher_key(master_seed);
        Ok(crypto::cipher::encrypt(bytes, &cipher_key.0, cipher)?)
    }

    pub(crate) fn write(&self, db: &mut File, buf: &[u8]) -> Result<(), Box<dyn Error>> {
        Ok(db.write_all(buf)?)
    }
}
