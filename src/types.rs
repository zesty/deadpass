use std::collections::HashMap;
use secrecy::Zeroize;
use rkyv::{Archive, Serialize, Deserialize};
use std::fmt::Debug;

pub trait SealedSecret {}

/// Store secrets in a hash
pub type SecretHash<T, Q> = HashMap<T, Q>;

impl<T: SealedSecret, Q: SealedSecret> SealedSecret for SecretHash<T, Q> {}

/// Store secrets in a vector
pub type SecretVec<T> = Vec<T>;

impl<T: SealedSecret> SealedSecret for SecretVec<T> {}

/// Type that holds values, all data must be erased and its immutable by default
/// Shows hidden when debugging
#[derive(Archive, Serialize, Deserialize)]
pub struct SecretVal<T: Debug + Zeroize>(pub T);

impl<T: Zeroize + Debug> Drop for SecretVal<T> {
    fn drop(&mut self) {
        self.0.zeroize();
    }
}

// Get inner type
impl<T: Zeroize + Debug> std::ops::Deref for SecretVal<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: Zeroize + Debug> SealedSecret for SecretVal<T> {}

impl<T: Zeroize + Debug> std::fmt::Debug for SecretVal<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #[cfg(feature = "visible")]
        {
            write!(f, "{:?}", &self.0)
        }

        #[cfg(not(feature = "visible"))]
        {
            write!(f, "\"*HIDDEN*\"")
        }
    }
}


// Implementation for tuples that may be needed

macro_rules! tuple_impls {
    ( $( $name:ident )+ ) => {
        impl<$($name: SealedSecret),+> SealedSecret for ($($name,)+) {}
    };
}

tuple_impls! { A }
tuple_impls! { A B }
tuple_impls! { A B C }
tuple_impls! { A B C D }
tuple_impls! { A B C D E }
tuple_impls! { A B C D E F }
tuple_impls! { A B C D E F G }
tuple_impls! { A B C D E F G H }
tuple_impls! { A B C D E F G H I }
tuple_impls! { A B C D E F G H I J }
tuple_impls! { A B C D E F G H I J K }
tuple_impls! { A B C D E F G H I J K L }
