use sodiumoxide::crypto::pwhash::argon2id13;
use std::error::Error;
use std::io::ErrorKind;
use secrecy::zeroize::Zeroize;

pub fn argon2id_hash(key: &[u8], salt: [u8; 16], opslimit: usize, memlimit: usize) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut hash = [0u8; argon2id13::HASHEDPASSWORDBYTES];
    let vec_hash = match argon2id13::derive_key(&mut hash, key, &argon2id13::Salt(salt), argon2id13::OpsLimit(opslimit), argon2id13::MemLimit(memlimit)) {
        Ok(v)  => v,
        Err(_) => {
		    return Err( Box::new( std::io::Error::new( ErrorKind::Other, "Error hashing and salting password" ) ) )
        }
    }.to_owned();
    hash.zeroize();

    Ok(vec_hash)
}
