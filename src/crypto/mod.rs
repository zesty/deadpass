pub(crate) mod argon2;
pub(crate) mod cipher;

use std::error::Error;
use std::io::ErrorKind;
use secrecy::Zeroize;
use sodiumoxide::crypto::pwhash;


#[repr(u8)]
#[derive(Clone)]
#[derive(Debug)]
pub(crate) enum Kdf {
    ARGON2ID { salt: [u8; 16], opslimit: u32, memlimit: u32 }
}

impl Drop for Kdf {
    fn drop(&mut self) {
        match self {
            &mut Self::ARGON2ID { mut salt, mut opslimit, mut memlimit } => {
                salt.zeroize();
                opslimit.zeroize();
                memlimit.zeroize();
            }
        }
    }
}

#[repr(u8)]
#[derive(Clone)]
#[derive(Debug)]
pub(crate) enum CiphFn {
    XCHACHA20POLY1305 { nonce_header: Option<[u8; 24]> }
}

impl Drop for CiphFn {
    fn drop(&mut self) {
        match self {
            &mut Self::XCHACHA20POLY1305 { mut nonce_header } => {
                nonce_header.zeroize();
            }
        }
    }
}


pub fn init() -> Result<(), Box<dyn Error>> {
    if sodiumoxide::init().is_err() {
        return Err( Box::new( std::io::Error::new( ErrorKind::Other, "Error init sodiumoxide" ) ) )
    }
    Ok(())
}

// Types used for api

pub enum KeyKdf {
    /// if opslimit and memlimit is bellow threshold, they will be ignored and give sane defaults
    ARGON2ID { opslimit: u32, memlimit: u32 }
}

impl KeyKdf {
    pub(crate) fn transform(&self) -> Kdf {
        match self {
            KeyKdf::ARGON2ID { opslimit, memlimit } => Kdf::ARGON2ID { salt: pwhash::argon2id13::gen_salt().0,
                                                               opslimit: if *opslimit < pwhash::argon2id13::OPSLIMIT_SENSITIVE.0 as u32 { pwhash::argon2id13::OPSLIMIT_SENSITIVE.0 as u32 } else { *opslimit },
                                                               memlimit: if *memlimit < pwhash::argon2id13::MEMLIMIT_SENSITIVE.0 as u32 { pwhash::argon2id13::MEMLIMIT_SENSITIVE.0 as u32 } else { *memlimit },
                                                             }
        }
    }
}

pub enum EncryptCipher {
    XCHACHA20POLY1305
}

impl EncryptCipher {
    pub(crate) fn transform(&self) -> CiphFn {
        match self {
            XCHACHA20POLY1305 => CiphFn::XCHACHA20POLY1305 { nonce_header: None }
        }
    }
}
