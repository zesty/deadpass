pub(crate) mod chacha;

use std::error::Error;

use crate::crypto::CiphFn;

pub(crate) fn decrypt(enc_bytes: &[u8], cipher_key: &[u8; 32], cipher: &CiphFn) -> Result<Vec<u8>, Box<dyn Error>> {
    match cipher {
        CiphFn::XCHACHA20POLY1305 { nonce_header } => Ok(chacha::decrypt(enc_bytes, &nonce_header, cipher_key)?)
    }
}

pub(crate) fn encrypt(bytes: &[u8], cipher_key: &[u8; 32], cipher: &CiphFn) -> Result<(Vec<u8>, CiphFn), Box<dyn Error>> {
    match cipher {
        CiphFn::XCHACHA20POLY1305 { nonce_header: _ } => Ok(chacha::encrypt(bytes, cipher_key)?)
    }
}
