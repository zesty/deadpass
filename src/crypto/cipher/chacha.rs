use sodiumoxide::crypto::secretstream;
use std::{io::ErrorKind, error::Error};

use crate::crypto::CiphFn;

pub(crate) fn decrypt(enc_bytes: &[u8], cipher_head: &Option<[u8; 24]>, cipher_key: &[u8; 32]) -> Result<Vec<u8>, Box<dyn Error>> {
    let key        = secretstream::Key(cipher_key.clone());
    let header     = secretstream::Header(cipher_head.clone().unwrap());
    let mut stream = match secretstream::Stream::init_pull(&header, &key) {
        Ok(v)  => v,
        Err(_) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error initializing xchacha20poly1305 stream decipher")))
    };
    
    if stream.is_finalized() {
        return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error decrypting database: Cipher stream already finalized")));
    }

    let (dec_bytes, _) = match stream.pull(&enc_bytes, None) {
        Ok(v)  => v, 
        Err(_) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error pulling from stream cipher")))
    };

    if !stream.is_finalized() {
        return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error decrypting database: Cipher stream not finalized after reading database")));
    }

    Ok(dec_bytes)
}

pub(crate) fn encrypt(bytes: &[u8], cipher_key: &[u8; 32]) -> Result<(Vec<u8>, CiphFn), Box<dyn Error>> {
    let key = secretstream::Key(cipher_key.to_owned());

    let (mut stream,
         header)   = match secretstream::Stream::init_push(&key) {
        Ok(v)  => v,
        Err(_) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error initializing xchacha20poly1305 stream cipher")))
    };

    let ciph = CiphFn::XCHACHA20POLY1305 { nonce_header: Some(header.0.to_owned()) };

    let enc_bytes = match stream.push(bytes, None, secretstream::Tag::Final) {
        Ok(v) => v,
        Err(_) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, "Error encrypting database body")))
    };

    Ok((enc_bytes, ciph))
}
